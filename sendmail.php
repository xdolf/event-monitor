<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require 'mail.conf.php';

// Load Composer's autoloader
//require 'vendor/autoload.php';

if($argc<4){
	exit("Usage: php sendmail.php event-id status\n");
}
$event_id=$argv[1];
$status=$argv[2];
$t_offline=$argv[3];
$day=floor($t_offline/86400);
$hour=floor($t_offline%86400/3600);
$minute=floor($t_offline%86400%3600/60);
$second=floor($t_offline%86400%3600%60);

//exit("event_id=".$event_id.",status=".$status."\n");

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.163.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = Username;                     // SMTP username
    $mail->Password   = Password;                               // SMTP password
    //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->SMTPSecure = "ssl";         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 465;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('xieshundao@163.com', 'Event Monitor');
    $mail->addAddress('xieshundao@qq.com', 'xieshundao');     // Add a recipient
    //$mail->addAddress('ellen@example.com');               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    // Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Event Monitor: status changed!'."Event $event_id is now $status.";
    $mail->Body    = "Event ID: $event_id </br>\n Status: $status </br>\n off line time: $day days $hour hours $minute minutes $second seconds</br>\n";
    #echo $mail->Subject;
    #echo $mail->Body;
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
?>
