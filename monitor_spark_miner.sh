#run this file with crontab every 20 minutes
# min hour day mon week command
#*/20  *   *   *    *   /var/www/html/event-monitor/monitor_spark_miner.sh  >> ~/monitor_wmc.log 2>&1

basepath=$(cd `dirname $0`;pwd)

d=$(date "+%Y-%m-%d %H:%M:%S")
echo $d

html=$(curl -s 'https://www.sparkpool.com/v1/miner/stats?miner=xdolf@outlook.com&pool=SPARK_POOL_CN&currency=GRIN_31' 2>/dev/null);
hashrate=$(echo "$html" | jq .data.hashrate)
n=$(echo "$html" | jq .data.onlineWorkerCount)
echo "hashrate($n online): $hashrate"

[ "s" != "s$n" ] && [ "s" != "s$hashrate" ] && [ $(expr $hashrate \> 0) -gt 0 ] && echo spark miner ok && exit 0

php ${basepath}/sendmail.php 'spark miner' $html 0 >> /dev/null
