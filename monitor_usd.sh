#run this file with crontab every 20 minutes
# min hour day mon week command
#*/20  *   *   *    *   /var/www/html/event-monitor/monitor_usd.sh  >> ~/monitor_usd.log 2>&1

basepath=$(cd `dirname $0`;pwd)
APPKEY=50739
SIGN=43db504a77f7d96b68839b1570becf31
price_th=700

echo price_th: $price_th

d=$(date "+%Y-%m-%d %H:%M:%S")
echo $d

price=$(curl -s "http://api.k780.com/?app=finance.rate_cnyquot&curno=USD&appkey=$APPKEY&sign=$SIGN&format=json"| jq .result.USD.BOC.se_sell | sed 's/"//g')
echo price: $price

[ "s" != "s$price" ] && [ $(expr $price \< $price_th) -gt 0 ] && echo price low && \
php ${basepath}/sendmail.php 'usd price' $price 0 >> /dev/null
