#run this file with crontab every 20 minutes
# min hour day mon week command
#*/20  *   *   *    *   /var/www/html/event-monitor/monitor.sh bypass > /dev/null 2>&1

basepath=$(cd `dirname $0`;pwd)
status_file="${basepath}/event-status.txt"

[ ! -f $status_file ] && echo "Error: status file $status_file not exist! Please update status before monitor." && exit 1
#30 min
off_line_time=1800

id=$1
[ "a$id" == "a" ] && echo "Error: event-id is null!" && exit 1

d=$(date "+%s")
hour=$(date "+%H")
d_pre=0
d_pre=$(awk '/^[0-9]{10,13} '${id}'$/{print $1}' $status_file)
#in case that the status is being updated while reading
[ $d_pre == 0 ] && sleep 5 && d_pre=$(awk '/^[0-9]{10,13} '${id}'$/{print $1}' $status_file)
d_diff=$((d-d_pre))

#echo $d $d_pre $d_diff

[ "$d_diff" -lt "$off_line_time" ] && echo "online" && exit 0

echo "offline"
#23:00-6:00 don't send mail
[ $hour -lt 6 -o $hour -ge 23 ] && eixt 0
php ${basepath}/sendmail.php $id offline $d_diff >> /dev/null
