
status_file="event-status.txt"
[ ! -f $status_file ] && echo "$status_file not exist, creating..." && cat <<EOF > $status_file
#this file and the director of this file  must be writable by webserver user(eg. apache)
#update-time-stamp event-id
EOF

#cat $status_file | awk '/^[^#]/{print "<tr><td>",$2,"</td><td>",strftime("%Y-%m-%d %H:%M:%S", $1),"</td><td>",(systime()-$1)/60, "min</td></tr>"}'
cat $status_file \
	| awk 'BEGIN{h=12;m=0;s=0;dt_th=3600*h+60*m+s;\
	printf "Offtime threshold: %d:%d:%d (%d seconds)\n",h,m,s,dt_th}/^[^#]/{\
	status="Offline";\
	dt=systime()-$1;\
	if(dt<dt_th){\
		status="Ok"\
	}\
	if(dt<3600){\
		tt=dt/60;\
		tstr="mins";\
	}else if(dt<3600*24){\
		tt=dt/3600;\
		tstr="hours"\
	}else{\
		tt=dt/3600/24;\
		tstr="days"\
	};\
	printf "<tr><td>%s</td><td>%s (%d %s ago)</td><td>%s</td></tr>\n",\
		$2,strftime("%Y-%m-%d %H:%M:%S", $1),tt,tstr,status}'

