
id=$1
[ "a$id" == "a" ] && echo "Error: event-id is null!" && exit 1

status_file="event-status.txt"
[ ! -f $status_file ] && echo "$status_file not exist, creating..." && cat <<EOF > $status_file
#this file and the director of this file  must be writable by webserver user(eg. apache)
#update-time-stamp event-id
EOF

#delete old status line
sed -i '/^[0-9]\{10,13\} '${id}'$/d' $status_file

#insert new status line
d=$(date "+%s")
echo "$d $id" >> $status_file
echo "Event \"$id\" updated sucessfully!"
