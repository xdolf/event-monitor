#run this file with crontab every 20 minutes
# min hour day mon week command
#*/20  *   *   *    *   /var/www/html/event-monitor/monitor_mwc_miner.sh  >> ~/monitor_mwc.log 2>&1

basepath=$(cd `dirname $0`;pwd)

d=$(date "+%Y-%m-%d %H:%M:%S")
echo $d

html=$(curl -s https://mwc.frostypool.com/ 2>/dev/null);
result=$(echo "$html" | awk '/title="idart \/ Workers: [1-9]"/{match($0,"<th>([0-9]+)</th>.*</td><td>([.0-9]+) / ([.0-9]+) gps</td>",arr);print arr[1],arr[2],arr[3]}')
echo result: $result

[ "s" != "s$result" ] && current_speed=$(echo "$result" | head -n 1 | awk '{print $2}') && echo current_speed: $current_speed
[ "s" != "s$current_speed" ] && [ $(expr $current_speed \> 0) -gt 0 ] && echo mwc miner speek ok && exit 0

php ${basepath}/sendmail.php 'mwc miner' "'${result}'" 0 >> /dev/null
