@echo off 
rem 定义需监控程序的进程名和程序路径，可根据需要进行修改
rem set AppName=Bypass.exe
rem set AppPath=C:\Users\LattePanda\Desktop\12306Bypass
rem title 进程监控
rem cls
rem echo.
rem echo 进程监控开始……
rem echo.
rem rem 定义循环体
rem :startjc
rem    rem 从进程列表中查找指定进程
rem    rem  下面语句也可写成 qprocess %AppName% >nul （经验发布后补充）
rem    tasklist|findstr /i %AppName% >nul
rem    rem 变量errorlevel的值等于0表示查找到进程，否则没有查找到进程
rem    if %errorlevel%==0 (
rem        echo ^>%date:~0,10% %time:~0,8% 程序正在运行……
rem        curl http://xdolf.idart.site:8080/event-monitor/update-status.php?id=bypass
rem    )else (
rem        echo ^>%date:~0,10% %time:~0,8% 没有发现程序进程
rem        rem echo ^>%date:~0,10% %time:~0,8% 正在重新启动程序
rem        rem start %AppPath%%AppName% 2>nul && echo ^>%date:~0,10% %time:~0,8% 启动程序成功
rem    )
rem    rem 用ping命令来实现延时运行
rem    for /l %%i in (1,1,10) do ping -n 1 -w 1000 www.sysu.edu.cn>nul
rem    goto startjc
rem    echo on
